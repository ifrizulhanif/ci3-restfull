<?php

defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Employee extends RestController
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Employee_model', 'employee');
	}

	public function index_get()
	{
		$id = $this->get('id');
		if ($id === null) {
			$employee = $this->employee->getEmployee();
		} else {
			$employee = $this->employee->getEmployee($id);
		}

		if ($employee) {
			$this->response([
				'status' => true,
				'data' => $employee
			], RestController::HTTP_OK);
		} else {
			$this->response([
				'status' => false,
				'message' => 'id not found'
			], RestController::HTTP_NOT_FOUND);
		}
	}

	public function index_post()
	{
		$data = [
			'name' => $this->post('name'),
			'email' => $this->post('email'),
			'password' => password_hash($this->post('password'), PASSWORD_BCRYPT),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		];

		if ($this->employee->createEmployee($data) > 0) {
			$this->response([
				'status' => true,
				'message' => 'new employee has been created'
			], RestController::HTTP_CREATED);
		} else {
			$this->response([
				'status' => false,
				'message' => 'failed to create new data'
			], RestController::HTTP_BAD_REQUEST);
		}
	}

	public function index_put()
	{
		$id = $this->put('id');
		$data = [
			'name' => $this->put('name'),
			'email' => $this->put('email'),
			'updated_at' => date('Y-m-d H:i:s')
		];

		if ($this->employee->updateEmployee($data, $id) > 0) {
			$this->response([
				'status' => true,
				'message' => 'new employee has been updated'
			], RestController::HTTP_OK);
		} else {
			$this->response([
				'status' => false,
				'message' => 'failed to update employee'
			], RestController::HTTP_BAD_REQUEST);
		}
	}

	public function index_delete()
	{
		$id = $this->delete('id');
		if ($id === null) {
			# code...
			$this->response([
				'status' => false,
				'message' => 'provide an id'
			], RestController::HTTP_BAD_REQUEST);
		} else {
			if ($this->employee->deleteEmployee($id) > 0) {
				# code...
				$this->response([
					'status' => true,
					'id' => $id,
					'message' => 'deleted'
				], RestController::HTTP_OK);
			} else {
				$this->response([
					'status' => false,
					'message' => 'id not found'
				], RestController::HTTP_BAD_REQUEST);
			}
		}
	}
}
