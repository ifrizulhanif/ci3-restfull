<?php

class Employee_model extends CI_Model
{
	public function getEmployee($id = null)
	{
		$this->db->select('name, email, created_at, updated_at');

		if ($id === null) {
			return $this->db->get('users')->result_array();
		} else {
			return $this->db->get_where('users', ['id' => $id])->result_array();
		}
	}

	public function createEmployee($data)
	{
		$this->db->insert('users', $data);
		return $this->db->affected_rows();
	}

	public function updateEmployee($data, $id)
	{
		// $this->db->set('name', $data['name']);
		// $this->db->set('email', $data['email']);
		// $this->db->set('updated_at', $data['updated_at']);
		// $this->db->where('id', $id);
		// $this->db->update('users');
		$this->db->update('users', $data, ['id' => $id]);
		return $this->db->affected_rows();
	}

	public function deleteEmployee($id)
	{
		$this->db->delete('users', ['id' => $id]);
		return $this->db->affected_rows();
	}
}
